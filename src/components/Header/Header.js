import React from "react";
import "./Header.scss";
import rasm from "../../image/rasm.png";
import frame from "../../image/Frame69.png";
import girl from "../../image/Ellipse57.png";

const Header = () => {
  return (
    <section className="header-section">
      <div className="header-container">
        <p className="header-title">Make your dream business goal come true</p>
        <p className="header-mini-title">
          when you need us for improve your business, then come with us to help
          your business have reach it, you just sit and feel that goal
        </p>
        <span className="header-button">Start Project</span>
        <div className="header-image-container">
          <div className="header-image">
            <img className="rasm" src={rasm} alt="" />
            <div className="frame">
            <span><i class="fa-solid fa-star"></i> Great Project</span>
            <p>800+ Done</p>
            </div>
            {/* <img className="frame" src={frame} alt="" /> */}
            <div className="header-image-comment">
              <div className="comment-header">
                <img src={girl} alt="" />
                <span>
                  <b>Bill Adams</b>
                  <p>CEO UpTech</p>
                </span>
              </div>
              <p className="comment-footer">
                “ This team is really the best in its field,I don't regret
                working with them, and will come back again thanks “
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Header;
