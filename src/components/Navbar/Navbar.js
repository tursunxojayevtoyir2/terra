import React, { useState } from "react";
import "./Navbar.scss";
import logo from "../../image/logo.svg";

const Navbar = () => {
  const [openContent, setOpenContent] = useState(true);
  return (
    <section className="navbar-section">
      <div className="nav-container">
        <div className="nav-logo">
          <img src={logo} alt="logo" />
        </div>
        <div className="nav-links">
          <ul className="nav-ul">
            <li className="nav-li">Direction</li>
            <li className="nav-li">Command</li>
            <li className="nav-li">Services</li>
            <li className="nav-li">Portfolio</li>
          </ul>
        </div>
        <div className="nav-contact dur">
          <span>Contact us</span>
          <i className="fa-solid fa-bars bar dur"
          onClick={()=>{
            setOpenContent(!openContent)
          }}
          ></i>
        </div>
      </div>
      <div className={`nav-mobile dur ${openContent ? "hidden" : "block"}`}>
        <ul className="nav-ul-mobile dur">
          <li className="nav-li-mobile">Direction</li>
          <li className="nav-li-mobile">Command</li>
          <li className="nav-li-mobile">Services</li>
          <li className="nav-li-mobile">Portfolio</li>
        </ul>
      </div>
    </section>
  );
};

export default Navbar;
