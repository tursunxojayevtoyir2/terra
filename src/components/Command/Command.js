import React from "react";
import "./Command.scss";
import Rectangle from "../../image/Rectangle832.png";
import Nuqta from "../../image/nuqta.png";

const Command = () => {
  return (
    <section className="command-section">
      <div className="command-container">
        <p className="command-command">Command</p>
        <p className="command-our">Our Teammate</p>
        <div className="command-image">
          <span>
            <img src={Rectangle} alt="" />
          </span>
          <img src={Nuqta} alt="" className="command-nuqta" />
          <img src={Nuqta} alt="" className="command-nuqta1" />
          <div className="command-image-title">
            <p>
              We move with make a Creative Strategy for help your business goal,
              we help to improve your income by a services we have. make your
              content look interesting and make people look for your business
            </p>
            <p>
              We move with make a Creative Strategy for help your business goal,
              we help to improve your income by a services we have. make your
              content look interesting and make people look for your business
            </p>
            <span className="command-button">
              <i class="fa-regular fa-circle-play"></i>Our Story
            </span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Command;
