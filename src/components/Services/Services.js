import React from "react";
import "./Services.scss";

const Services = () => {
  return (
    <section className="services-section">
      <div className="services-container">
        <div className="services-text">
          <p className="services-our">Our Services</p>
          <p className="services-perfect">Perfect and Fast Movement</p>
          <p className="services-title">
            We move with make a Creative Strategy for help your business goal,
            we help to improve your income by a services we have. make your
            content look interesting and make people look for your business
          </p>
        </div>
        <div className="services-category">
          <div className="category-item">
            <span className=" item-1">
            <i className="fa-solid fa-bullhorn"></i>
            </span>
            <p>Social Media Management</p>
          </div>
          <div className="category-item">
            <span className=" item-2">
              <i className="fa-solid fa-gear"></i>
            </span>
            <p>Search Engine Opimization</p>
          </div>
          <div className="category-item">
            <span className=" item-3">
              <i className="fa-solid fa-pen-nib"></i>
            </span>
            <p>Design</p>
          </div>
          <div className="category-item">
            <span className=" item-4">
            <i className="fa-solid fa-tv"></i>
            </span>
            <p>Ads</p>
          </div>
          <div className="category-item">
            <span className=" item-5">
            <i className="fa-solid fa-code"></i>
            </span>
            <p>Websites</p>
          </div>
          <div className="category-item">
            <span className=" item-6">
            <i className="fa-solid fa-mobile-screen-button"></i>
            </span>
            <p>Mobile Application</p>
          </div>
          <div className="category-item">
            <span className=" item-7">
            <i className="fa-solid fa-share-nodes"></i>
            </span>
            <p>Crossplatforms</p>
          </div>
          <div className="category-item">
            <span className=" item-8">
            <i className="fa-solid fa-desktop"></i>
            </span>
            <p>Desktop</p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Services;
