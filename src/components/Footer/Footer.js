import React from "react";
import "./Footer.scss";
import logo from "../../image/logo.png";

const Footer = () => {
  return (
    <section className="footer-section">
      <div className="footer-container">
        <div className="main-footer">
          <div className="footer-logo">
            <img src={logo} alt="logo" />
          </div>
          <ul className="footer-ul-1">
            <li>
              <b>Terms & policies</b>
            </li>
            <li>Terms of service</li>
            <li>Privacy Policy</li>
          </ul>
          <ul className="footer-ul-2">
            <li>
              <b>Company</b>
            </li>
            <li>Direction</li>
            <li>Command</li>
            <li>Contact Us</li>
          </ul>
          <ul className="footer-ul-3">
            <li>
              <b>Contact</b>
            </li>
            <li>Contact (+998) 99 777 77 77</li>
            <li> terratech@gmail.com</li>
          </ul>
          <ul className="footer-ul-4">
            <li>
              <b>Location</b>
            </li>
            <li>Samarkand, Uzbekistan Bagishamol 45-uy</li>
          </ul>
        </div>
        <div className="footer-icons" >
          <div className="icons" >
            <i className="fa-brands fa-facebook"></i>
            <i className="fa-brands fa-instagram"></i>
            <i className="fa-brands fa-linkedin"></i>
            <i className="fa-regular fa-envelope"></i>
            <i className="fa-brands fa-twitter-square"></i>
          </div>
          <p>Copyright @ 2022 Terra Tech Company All Right Reserved</p>
        </div>
      </div>
    </section>
  );
};

export default Footer;
