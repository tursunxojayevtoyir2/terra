import React from "react";
import "./About.scss";
import girl from "../../image/Ellipse57.png";
import nuqta from "../../image/nuqta.png"

const About = () => {
  return (
    <section className="about-section">
      <div className="about-container">
        <p className="about-title">Testimonial</p>
        <p className="about-title1">People Talk about us</p>
        <div className="about-comment">
          <div className="header-image-comment">
            <div className="comment-header">
              <img src={girl} alt="" />
              <span>
                <b>Angel Rose</b>
                <p>Creative Manager</p>
              </span>
            </div>
            <p className="comment-footer">
              There are many variations passages of Lorem Ipsum majority some by
              words which don't look .
            </p>
          </div>
          <div className="header-image-comment">
            <div className="comment-header">
              <img src={girl} alt="" />
              <span>
                <b>Angel Rose</b>
                <p>Creative Manager</p>
              </span>
            </div>
            <p className="comment-footer">
              There are many variations passages of Lorem Ipsum majority some by
              words which don't look .
            </p>
          </div>
          <div className="header-image-comment">
            <div className="comment-header">
              <img src={girl} alt="" />
              <span>
                <b>Angel Rose</b>
                <p>Creative Manager</p>
              </span>
            </div>
            <p className="comment-footer">
              There are many variations passages of Lorem Ipsum majority some by
              words which don't look .
            </p>
          </div>
          <img className="comment-nuqta" src={nuqta} alt="nuqta" />
          <img className="comment-nuqta1" src={nuqta} alt="nuqta" />
        </div>
      </div>
    </section>
  );
};

export default About;
