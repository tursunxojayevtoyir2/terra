import React, { useState } from "react";
import "./Portfolio.scss";
import img1 from "../../image/img1.png";
import img2 from "../../image/img2.png";
import img3 from "../../image/img3.png";
import nuqta from "../../image/nuqta.png";

const Portfolio = () => {
  const [openContent, setOpenContent] = useState(true);
  return (
    <section className="portfolio-section">
      <div className="portfolio-container">
        <div className="portfolio-text">
          <p className="portfolio-our">Our portfolio</p>
          <p className="portfolio-what">What do we do</p>
          <p className="portfolio-title">
            all projects that we have already done , proven can help to use more
            comfortable, then can used by client from our business
          </p>
        </div>
        <div className="portfolio-images">
          <img className="portfolio-img" src={img1} alt="images" />
          <img className="portfolio-img" src={img2} alt="images" />
          <img className="portfolio-img" src={img3} alt="images" />
          <img className="nuqta" src={nuqta} alt="images" />
          <img className="nuqta1" src={nuqta} alt="images" />
        </div>
        <div className={`portfolio-images ${openContent ? "hidden" : "block"}`}>
          <img className="portfolio-img" src={img1} alt="images" />
          <img className="portfolio-img" src={img2} alt="images" />
          <img className="portfolio-img" src={img3} alt="images" />
          <img className="nuqta" src={nuqta} alt="images" />
          <img className="nuqta1" src={nuqta} alt="images" />
        </div>
        <div className={`portfolio-images ${openContent ? "hidden" : "block"}`}>
          <img className="portfolio-img" src={img1} alt="images" />
          <img className="portfolio-img" src={img2} alt="images" />
          <img className="portfolio-img" src={img3} alt="images" />
          <img className="nuqta" src={nuqta} alt="images" />
          <img className="nuqta1" src={nuqta} alt="images" />
        </div>
        <span
          className="portfolio-button"
          onClick={() => {
            setOpenContent(!openContent);
          }}
        >
          See All Portfolio
        </span>
      </div>
    </section>
  );
};

export default Portfolio;
