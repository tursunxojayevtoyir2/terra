import React from "react";
import "./Contact.scss";

const Contact = () => {
  return (
    <section className="contact-section">
      <div className="contact-container">
        <p className="contact-title">Contact Us</p>
        <p className="contact-text">
          Aenean sit amet magna vel magna fringilla fermentum. Donec sit amet
          nulla sed arcu pulvinar ultricies commodo id ligula.
        </p>
        <div className="contact-inputs">
          <div className="input-four">
            <input
              className="input1"
              type="text"
              placeholder="What is your name?"
            />
            <input
              className="input2"
              type="email"
              placeholder="What is your email?"
            />
            <input
              className="input3"
              type="text"
              placeholder="What is your phone number?"
            />
          </div>
          {/* <div className="input-message">
            <p>Write your message here</p>
            <input type="text" />
          </div> */}
          <div className="contact-button">
              <span>Send Message</span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
